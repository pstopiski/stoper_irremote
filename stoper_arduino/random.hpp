#pragma once

#include <memory>
#include <chrono>

namespace stoper_arduino
{
    std::unique_ptr<std::mt19937> make_seeded_mt19937()
    {
        // http://www.pcg-random.org/posts/simple-portable-cpp-seed-entropy.html
        // note that seed_seq may be weak
        std::seed_seq seq{
            (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF),
            (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF),
            (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF),
            (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF), (unsigned)secureRandom(0xFFFFFFFF),
            (unsigned)millis(), (unsigned)micros() };
        std::unique_ptr<std::mt19937> result{ new std::mt19937(seq) };
        return std::move(result);
    }

}
