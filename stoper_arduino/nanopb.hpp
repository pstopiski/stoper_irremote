#pragma once

#include "../pb_encode.h"

namespace stoper_arduino
{
    inline bool write_string(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
    {
        const char *str = (const char *)*arg;
        if (!pb_encode_tag_for_field(stream, field))
            return false;

        return pb_encode_string(stream, (uint8_t*)str, strlen(str));
    }

    inline pb_callback_t make_write_string_callback(const char *text)
    {
        pb_callback_t res;
        res.arg = (void*)text;
        res.funcs.encode = write_string;
        return res;
    }
}
