#pragma once

#include <chrono>

namespace stoper_arduino
{
    /**
    * @brief Clock based on millis(), micros()
    *
    * The now() method must be called at least every 30 days.
    */
    struct clock_t
    {
        typedef std::chrono::nanoseconds duration;
        typedef duration::rep rep;
        typedef duration::period period;
        typedef std::chrono::time_point<::stoper_arduino::clock_t, duration> time_point;

        static constexpr bool is_steady = true;

        clock_t(duration start_time = std::chrono::seconds(0)) :m_last_micros(micros()), m_last_millis(millis())
        {
            m_last += start_time;
        }

        // Needs to be called at least every 30 minutes for microsecond accuracy, and at most every 30 days for millisecond.
        time_point now() noexcept
        {
            update_time();
            return m_last;
        }

    private:
        typedef decltype(micros()) micros_t;
        typedef decltype(millis()) millis_t;

        template<typename T>
        static T elapsed(T const &last, T const &now)
        {
            if (last <= now)
                return now - last;
            else
                return (std::numeric_limits<T>::max() - last) + now;
        }

        void update_time()
        {
            auto const current_millis = millis();
            auto const elapsed_millis = elapsed(m_last_millis, current_millis);
            auto const current_micros = micros();

            if (elapsed_millis < 20 * 60 * 1000)
            {
                // if less then 20 minutes elapsed we can rely on microsecond timer for better accuracy
                m_last += std::chrono::microseconds(elapsed(m_last_micros, current_micros));
            }
            else
            {
                m_last += std::chrono::milliseconds(elapsed_millis);
            }

            m_last_millis = current_millis;
            m_last_micros = current_micros;
        }

        time_point m_last;
        micros_t m_last_micros;
        millis_t m_last_millis;
    };

}
