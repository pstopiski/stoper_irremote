#include <IRremoteESP8266.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <string>
#include <chrono>
#include <random>
#include <memory>
#include "wifi_data.h" // wifi_ssid, wifi_password, mqtt_host
#include "stoper_arduino/clock.hpp"
#include "stoper_arduino/random.hpp"
#include "stoper_arduino/nanopb.hpp"
#include <boost/mpl/if.hpp>
#include <boost/mpl/list.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/transition.hpp>
#include "basic_device_info.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"

/*

stoper_home/
- root for devices

stoper_home/esp8266_irremote_2324353245_23452342
- device root for device session
- message content: basic_device_info.pb

stoper_home/esp8266_irremote_2324353245_23452342/requests
- place to add requests for device

stoper_home/esp8266_irremote_2324353245_23452342/disconnected
- last will
- message content: "unexpected"
- garbage collector should remove all data for this device session when sees it


*/

namespace
{
    static const int RECV_PIN = 2; //an IR detector/demodulatord is connected to GPIO pin 2
    static const int SEND_PIN = 5; //an IR LED is connected to GPIO pin 5

    IRrecv irrecv(RECV_PIN);
    IRsend irsend(SEND_PIN);

    static const unsigned long MinPressDelay = 200;
    static const unsigned long HumanPressDelay = 1000;

    std::string mqtt_runtime_id;
    std::string mqtt_runtime_id_prefix("esp8266_irremote");
    std::string const mqtt_stoper_home_root("stoper_home/");



    struct ev_delay_elapsed : ::boost::statechart::event<ev_delay_elapsed>
    {
    };

    struct ev_mqtt_connected : ::boost::statechart::event<ev_mqtt_connected>
    {
    };

    struct ev_mqtt_disconnected : ::boost::statechart::event<ev_mqtt_disconnected>
    {
    };

    struct ev_mqtt_generic_request : ::boost::statechart::event<ev_mqtt_generic_request>
    {
        std::uint32_t m_message_id;
        std::vector<std::uint8_t> m_payload;
    };

    struct st_disconnected;
    struct sm_mqtt : boost::statechart::state_machine<sm_mqtt, st_disconnected>
    {
        sm_mqtt(PubSubClient &mqtt, ::stoper_arduino::clock_t &clock) :m_mqtt(mqtt), m_clock(clock)
        {
            m_mqtt.setCallback([this](char* topic, byte* payload, unsigned int length) {
                Serial.print("MQTT message arrived [");
                Serial.print(topic);
                Serial.print("] ");
                for (int i = 0; i < length; i++) {
                    char receivedChar = (char)payload[i];
                    Serial.print(receivedChar);
                }
                Serial.println();

                if (length > 0)
                {
                    request_header_t message_header = request_header_t_init_zero;
                    pb_istream_t stream = pb_istream_from_buffer(payload, length);
                    if (pb_decode(&stream, request_header_t_fields, &message_header))
                    {
                        boost::intrusive_ptr<ev_mqtt_generic_request> req(new ev_mqtt_generic_request);
                        req->m_message_id = message_header.message_id;
                        req->m_payload.resize(length);
                        std::copy_n(payload, length, &req->m_payload[0]);
                        this->process_event(*req);
                    }
                }
            });
        }

        PubSubClient &m_mqtt;
        ::stoper_arduino::clock_t &m_clock;
    };

    struct st_connected;
    struct st_disconnected : boost::statechart::simple_state<st_disconnected, sm_mqtt>
    {
        typedef ::boost::mpl::list<
            ::boost::statechart::custom_reaction<ev_delay_elapsed>,
            ::boost::statechart::transition<ev_mqtt_connected, st_connected>
        > reactions;

        ::boost::statechart::result react(const ev_delay_elapsed &)
        {
            auto &mqtt = outermost_context().m_mqtt;
            auto const now = outermost_context().m_clock.now();

            if (now - m_last_connect_tried > std::chrono::seconds(2))
            {
                Serial.print("Attempting Boost Statechart MQTT connection...");
                if (mqtt.connect(mqtt_runtime_id.c_str(), (mqtt_stoper_home_root + mqtt_runtime_id + "/disconnected").c_str(), 0, true, "unexpected"))
                {
                    Serial.println("connected");
                    return transit<st_connected>();
                }
                else
                {
                    Serial.print("failed, rc=");
                    Serial.println(mqtt.state());
                }

                m_last_connect_tried = now;
            }
            return discard_event();
        }

    private:
        ::stoper_arduino::clock_t::time_point m_last_connect_tried;

    };

    struct st_caps_sent;
    struct st_connected : boost::statechart::state<st_connected, sm_mqtt>
    {
        struct ev_mqtt_caps_sent : ::boost::statechart::event<ev_mqtt_caps_sent>
        {
        };

        typedef ::boost::mpl::list<
            ::boost::statechart::custom_reaction<ev_mqtt_generic_request>,
            ::boost::statechart::transition<ev_mqtt_disconnected, st_disconnected>,
            ::boost::statechart::transition<ev_mqtt_caps_sent, st_caps_sent>
        > reactions;

        st_connected(my_context ctx) :state(ctx)
        {
            auto &mqtt = outermost_context().m_mqtt;

            bool pushed_all = true;
            {
                // Publish basic device info

                basic_device_info_t mymessage = {  };
                mymessage.interface_type = stoper_arduino::make_write_string_callback("irremote");
                mymessage.interface_version = 1;
                mymessage.product_code = stoper_arduino::make_write_string_callback("esp8266");

                pb_ostream_t sizestream = { 0 };
                pb_encode(&sizestream, basic_device_info_t_fields, &mymessage);
                uint8_t buffer[sizestream.bytes_written];
                pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
                pb_encode(&stream, basic_device_info_t_fields, &mymessage);

                pushed_all = pushed_all && mqtt.publish((mqtt_stoper_home_root + mqtt_runtime_id).c_str(), buffer, stream.bytes_written, true);

                // Publish last will so that on disconnection it can be garbage collected

                
            }

            pushed_all = pushed_all && mqtt.subscribe((mqtt_stoper_home_root + mqtt_runtime_id + "/requests/+").c_str());
            pushed_all = pushed_all && mqtt.publish((mqtt_stoper_home_root + mqtt_runtime_id + "/requests").c_str(), "Place requests here", true);
            if (pushed_all)
                post_event(ev_mqtt_caps_sent());
            else
                post_event(ev_mqtt_disconnected());
        }

        ::boost::statechart::result react(const ev_mqtt_generic_request &req)
        {
            switch (static_cast<message_id_t>(req.m_message_id))
            {
            case message_id_t_SET_DEVICE_NAME_REQ: {
                Serial.print("Got request to set device name TODO");
                break;
            }
            }
            return forward_event();
        }

    };

    struct st_caps_sent : boost::statechart::simple_state<st_caps_sent, sm_mqtt>
    {
        typedef ::boost::mpl::list<
            ::boost::statechart::custom_reaction<ev_delay_elapsed>,
            ::boost::statechart::transition<ev_mqtt_connected, st_connected>
        > reactions;

        st_caps_sent()
        {
            Serial.println("Yay! Caps sent");
        }

        ::boost::statechart::result react(const ev_delay_elapsed &)
        {
            auto &mqtt = outermost_context().m_mqtt;
            mqtt.loop();

            if (mqtt.connected())
                return discard_event();
            else
                return transit<st_disconnected>();
        }
    };


    struct stoper_ir_remote_t
    {
        stoper_ir_remote_t() :m_rnd(::stoper_arduino::make_seeded_mt19937()), m_mqtt(m_espClient), m_clock(std::chrono::milliseconds(millis())), m_sm_mqtt(m_mqtt, m_clock)
        {
            mqtt_runtime_id.resize(40);
            mqtt_runtime_id.resize(sprintf((char*)mqtt_runtime_id.data(), "%s_%u_%u", mqtt_runtime_id_prefix.c_str(), (unsigned)(*m_rnd)(), (unsigned)(*m_rnd)()));

            m_mqtt.setServer(mqtt_host, 1883);

            m_sm_mqtt.initiate();
        }

        void loop()
        {
            auto const now = m_clock.now();

            if (now > m_next_minute_elapsed)
            {
                print_time();

                m_next_minute_elapsed = now + std::chrono::minutes(1);
            }

            m_sm_mqtt.process_event(ev_delay_elapsed());

            decode_results results;
            if (irrecv.decode(&results)) {
                Serial.println(results.value, HEX);
                print_time();

                if (results.value == 0xC2CA827D || results.value == 0x20DF8D72)
                {
                    m_mqtt.publish((mqtt_stoper_home_root + mqtt_runtime_id + "/button").c_str(), "1", true);

                    ir_switch_to_radio();
                }
                else if (results.value == 0xC2CA42BD || results.value == 0x20DF0DF2)
                {
                    m_mqtt.publish((mqtt_stoper_home_root + mqtt_runtime_id + "/button").c_str(), "2", true);

                    ir_switch_to_aux();
                }
                else if (results.value == 0x20DF5DA2)
                {
                    m_mqtt.publish((mqtt_stoper_home_root + mqtt_runtime_id + "/button").c_str(), "3", true);

                    ir_toggle_power();
                }

                irrecv.resume(); // Receive the next value
            }
            delay(100);
        }

        void ir_toggle_power()
        {
            Serial.println("'3' pressed, power ON/OFF");
            delay(HumanPressDelay);
            irsend.sendSAMSUNG(0xC2CA807F, 32); //Power
        }

        void ir_switch_to_aux()
        {
            Serial.println("'2' pressed, changing to AUX");
            delay(HumanPressDelay);
            irsend.sendSAMSUNG(0xC2CA817E, 32); //DVD
            delay(MinPressDelay);
            irsend.sendSAMSUNG(0xC2CA8877, 32); // D.IN
            delay(MinPressDelay);
            irsend.sendSAMSUNG(0xC2CA8877, 32); // AUX
        }

        void ir_switch_to_radio()
        {
            Serial.println("'1' pressed, changing to radio");
            delay(HumanPressDelay);
            irsend.sendSAMSUNG(0xC2CA817E, 32); //DVD
            delay(MinPressDelay);
            irsend.sendSAMSUNG(0xC2CA8877, 32); // D.IN
            delay(MinPressDelay);
            irsend.sendSAMSUNG(0xC2CA8877, 32); // AUX
                                                //delay(MinPressDelay);
            delay(2500); // workaround for slow Samsung
            irsend.sendSAMSUNG(0xC2CA8877, 32); // IPOD
                                                //delay(MinPressDelay);
            delay(1000); // workaround for slow Samsung
            irsend.sendSAMSUNG(0xC2CA8877, 32); // FM
        }

        void print_time()
        {
            auto since_epoch = m_clock.now().time_since_epoch();
            auto hours = std::chrono::duration_cast<std::chrono::hours>(since_epoch).count();
            auto minutes = std::chrono::duration_cast<std::chrono::minutes>(since_epoch -= std::chrono::hours(hours)).count();
            auto seconds = std::chrono::duration_cast<std::chrono::seconds>(since_epoch -= std::chrono::minutes(minutes)).count();
            auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch -= std::chrono::seconds(seconds)).count();
            Serial.printf("Time since boot: %uh %um %us %ums, heap: %u\n", (std::uint32_t)hours, (std::uint32_t)minutes, (std::uint32_t)seconds, (std::uint32_t)milliseconds, (std::uint32_t)ESP.getFreeHeap());
        }

        void try_reconnect()
        {
            Serial.print("Attempting MQTT connection...");
            if (m_mqtt.connect(mqtt_runtime_id.c_str()))
            {
                Serial.println("connected");
                m_mqtt.subscribe("ledStatus");
            }
            else {
                Serial.print("failed, rc=");
                Serial.println(m_mqtt.state());
            }
        }

    private:
        std::unique_ptr<std::mt19937> const m_rnd;
        WiFiClient m_espClient;
        PubSubClient m_mqtt;
        ::stoper_arduino::clock_t m_clock;
        ::stoper_arduino::clock_t::time_point m_next_minute_elapsed;
        sm_mqtt m_sm_mqtt;

    };

}

stoper_ir_remote_t *stoper_ir_remote = nullptr;

void setup()
{
    typename boost::mpl::if_c<true, int, bool>::type i = 1;

    WiFi.mode(WIFI_STA);
    WiFi.begin(wifi_ssid, wifi_password);

    Serial.begin(9600);

    Serial.printf("Version 5, heap size: %u\n", ESP.getFreeHeap());

    irrecv.enableIRIn(); // Start the receiver
    irsend.begin();

    stoper_ir_remote = new stoper_ir_remote_t;
}

void loop()
{
    stoper_ir_remote->loop();
}
