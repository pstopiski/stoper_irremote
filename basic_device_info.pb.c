/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.8 at Wed Apr 19 22:25:21 2017. */

#include "basic_device_info.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t device_name_t_fields[3] = {
    PB_FIELD(  1, STRING  , SINGULAR, CALLBACK, FIRST, device_name_t, device_id, device_id, 0),
    PB_FIELD(  2, STRING  , SINGULAR, CALLBACK, OTHER, device_name_t, device_label, device_id, 0),
    PB_LAST_FIELD
};

const pb_field_t basic_device_info_t_fields[6] = {
    PB_FIELD(  1, MESSAGE , SINGULAR, STATIC  , FIRST, basic_device_info_t, name, name, &device_name_t_fields),
    PB_FIELD(  2, STRING  , SINGULAR, CALLBACK, OTHER, basic_device_info_t, interface_type, name, 0),
    PB_FIELD(  3, INT32   , SINGULAR, STATIC  , OTHER, basic_device_info_t, interface_version, interface_type, 0),
    PB_FIELD(  4, STRING  , SINGULAR, CALLBACK, OTHER, basic_device_info_t, product_code, interface_version, 0),
    PB_FIELD(  5, STRING  , SINGULAR, CALLBACK, OTHER, basic_device_info_t, serial_number, product_code, 0),
    PB_LAST_FIELD
};

const pb_field_t device_connection_info_t_fields[2] = {
    PB_FIELD(  1, UENUM   , SINGULAR, STATIC  , FIRST, device_connection_info_t, connection_state, connection_state, 0),
    PB_LAST_FIELD
};

const pb_field_t detection_time_t_fields[2] = {
    PB_FIELD(  1, STRING  , SINGULAR, CALLBACK, FIRST, detection_time_t, date_time, date_time, 0),
    PB_LAST_FIELD
};

const pb_field_t request_header_t_fields[2] = {
    PB_FIELD(  1, UINT32  , SINGULAR, STATIC  , FIRST, request_header_t, message_id, message_id, 0),
    PB_LAST_FIELD
};

const pb_field_t set_device_name_req_t_fields[3] = {
    PB_FIELD(  1, MESSAGE , SINGULAR, STATIC  , FIRST, set_device_name_req_t, header, header, &request_header_t_fields),
    PB_FIELD(  2, MESSAGE , SINGULAR, STATIC  , OTHER, set_device_name_req_t, name, header, &device_name_t_fields),
    PB_LAST_FIELD
};




/* Check that field information fits in pb_field_t */
#if !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_32BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 * 
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in 8 or 16 bit
 * field descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(basic_device_info_t, name) < 65536 && pb_membersize(set_device_name_req_t, header) < 65536 && pb_membersize(set_device_name_req_t, name) < 65536), YOU_MUST_DEFINE_PB_FIELD_32BIT_FOR_MESSAGES_device_name_t_basic_device_info_t_device_connection_info_t_detection_time_t_request_header_t_set_device_name_req_t)
#endif

#if !defined(PB_FIELD_16BIT) && !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_16BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 * 
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in the default
 * 8 bit descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(basic_device_info_t, name) < 256 && pb_membersize(set_device_name_req_t, header) < 256 && pb_membersize(set_device_name_req_t, name) < 256), YOU_MUST_DEFINE_PB_FIELD_16BIT_FOR_MESSAGES_device_name_t_basic_device_info_t_device_connection_info_t_detection_time_t_request_header_t_set_device_name_req_t)
#endif


/* @@protoc_insertion_point(eof) */
